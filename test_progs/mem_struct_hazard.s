/*
Simplest program that will cause a structural hazard between I and D mem 
*/
.section .text
.align 4

data = 0xdead
addi t2, x0, 1024
lui t3, data
nop
nop
nop
nop
nop
sw t3, 0(t2)
nop
add t2, t2, t2
nop
nop
nop
wfi
